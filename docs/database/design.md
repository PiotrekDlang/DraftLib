## About

This is a project of a database engine embedded into the D language. It doesn't use any external libraries and SW. It also doesn't act as an interface/driver to other database engines. 

It can be seen as persistence storage for array of objects. However there are  additional concepts incorporated from database domain. Queries are constructed by D syntax (e.g. using std.algorithm) mainly via range interface. 

## Architecture

The D database engine design is inspired by SQLite database backend. 
Only some concepts are similar. In general implementation of the storage engine is not compatible with SQLite in any way.
Because there is no translation to SQL or any other intermediate language, a query execution is managed directly by user code (with the exception of index (lookup) handling which is performed by the storage engine). 

![Architecture](database_arch.png)

The Storage API is meant to be simple for seamless storage backend replacement.

## File format

Database storage is based on paging. Its current status is in early stage.

![File format](storage_design.png)

## Key features

* ACID (not implemented yet)
* Lack of domain specific language (e.g. SQL)
* Fast (not implemented yet)
* Full compatibility with D algorithms and ranges
* Suitable for embedded systems (not implemented yet)
* Minimal overhead: nothrow @nogc (not implemented yet)
* Minimal storage size in one file (not implemented yet)

## Disadvantages
* Index usage requires strings and CTFE (e.g. filter!q{item.id <10 && item.group == "AB"})
* The RPC nature of SQL queries are not possible without additional functionality and currently there are no plans to allow it.


## Initial decisions and their justification. 
Everything is subject to change.


### API
* Database
    * db management (create, open, close, delete, etc,)
* Collection
    * range API (empty, pop, front, etc.)
    * custom API 
* Item
    * itemId

### File format 
* one file
* divided into pages
* little endian data format
* variable length object storage (cells)

### Page Format
* page contains header and payload
* payload area contains fixed number of same size slots
* slot can be either data or a pointer 
    * if type size is greater than pointer size - pointer is used
    * else raw data is put into the slot

### Pointer Format
* flag
* offset
* pageId

Or (depending on flags)

* flag
* data

### Cell Format
* variable-length integer

### Cell Lookup
* by offset computed from itemId
* if available guided by indexes (challenging)

#### Types
* Allowed types
    * classes
    * structs
    * enumns
    * scalar types
    * arrays
* direct or via proxy object retrieval patterns
* accessing members by value or reference
    * member object embedded in the same table or separate
* implicit conversion to string or string[string]
    * usable for unknown types
* name checking of object members
    * fields
    * types
    * number of members